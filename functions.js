

// @ts-check

// const person = {
//     firstName:"John",
//     lastName: "Doe",
//     fullName: function(city, country) {
//       console.log(this.firstName + " " + this.lastName + "," + city + "," + country);
//     }
//   }
  
  
  
//   person.fullName("Lahore", "Pakistan");

   

/** 
 * Represents addition.
 * @function Addition
 * params {number} n1 - First Number
 * params {number} n2 - Second Number */
function addition(n1,n2) {
    console.log(n1+n2);
 }
/** Calling addition function */
addition(3,2);

/** 
 * Represents product.
 * @function Product
 * params {number} n1 - First Number
 * params {number} n2 - Second Number */
 function Product(n1,n2) {
    console.log(n1*n2);
 }/** Calling addition function */
Product(3,2);
 


/**
 * calculate Object
 * @typedef {Object} - calculate
 * @param {Number} l - length
 * @param {Number} w - width
 * @param {{h1: number , h2:number }}h - heigth 
 * @returns {number} - Some arithmetic operation
 */
 
 const calculate = (l, w, h = {
    h1: 2,
    h2: 4
 }) => {

   return (l*w+h. h1) - h.h2;  
 }

 /**
  * calling the function and printing the value
  */
 console.log (calculate (12, 45, { h1: 2,  h2: 4}));
